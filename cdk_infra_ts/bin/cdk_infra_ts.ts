#!/usr/bin/env node
import * as cdk from 'aws-cdk-lib';
import { CdkInfraTsStack } from '../lib/cdk_infra_ts-stack';

const app = new cdk.App();
new CdkInfraTsStack(app, 'CdkInfraTsStack');
