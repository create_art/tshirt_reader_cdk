import { Duration, Stack, StackProps } from 'aws-cdk-lib';
import * as dynamodb from 'aws-cdk-lib/aws-dynamodb';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as apigw from 'aws-cdk-lib/aws-apigateway'
import { CloudFrontToS3, CloudFrontToS3Props } from '@aws-solutions-constructs/aws-cloudfront-s3';

import { Construct } from 'constructs';

export class CdkInfraTsStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    // TODO: Add Cloudfront_S3 for app
    // https://github.com/awslabs/aws-solutions-constructs/tree/main/source/patterns/@aws-solutions-constructs/aws-cloudfront-s3
    const frontendApp = new CloudFrontToS3(this, 'tshirt-frontend', {});

    // TODO: Add DynamoDB for Storage layer
    const tableName = 'tshirt_table_cdk';

    const table = new dynamodb.Table(this, tableName, {
      partitionKey: { name: 'tshirt_id', type: dynamodb.AttributeType.STRING },
      sortKey: { name: 'timestamp', type: dynamodb.AttributeType.NUMBER },
      readCapacity: 5,
      writeCapacity: 5,
      tableName,
    });

    // TODO: Add Lambda for REST API
    const tshirt_lambda = new lambda.Function(this, 'TShirtHandler', {
      runtime: lambda.Runtime.NODEJS_18_X, // latest exec environment
      code : lambda.Code.fromAsset('tshirt_lambda'), // Point to code folder
      handler : 'tshirt_lambda.handler'
    });

    // TODO: Add API Gateway for API routing
    new apigw.LambdaRestApi(this, 'TShirtEndpoint', {
      handler: tshirt_lambda
    });
  }
}
