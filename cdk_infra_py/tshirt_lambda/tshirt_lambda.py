import json
import os
import boto3

def lambda_handler(event, context):
    # TODO: implement image handling for shirts
    print(event)
    
    data = json.loads(event["body"])

    dynamodb = boto3.resource('dynamodb')
    TABLE_NAME = os.environ['TABLE_NAME']
    table = dynamodb.Table(TABLE_NAME)
    
    table.put_item(Item={
                "tshirt_id": data["tshirt_id"],
                "name": data["name"],
            }
    )
    return {
        'statusCode': 200,
        'body': json.dumps('TShirt Added!')
    }