from aws_cdk import (
    Duration,
    Stack,
    aws_dynamodb as dynamodb_cdk,
    aws_lambda as lambda_cdk,
    aws_apigateway as apigateway_cdk,
)
from constructs import Construct
from aws_solutions_constructs.aws_cloudfront_s3 import CloudFrontToS3

class CdkInfraStack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)
        
        # The code that defines your stack goes here
        cf_s3 = CloudFrontToS3(self, 'tshirt-cloudfront-s3')


        # APIG-Lambda-DDB stack based on :
        # https://github.com/fengliplatform/cdk-apigw-lambda-dynamodb/
        # Create DynamoDB table
        tshirt_table_cdk = dynamodb_cdk.Table(self, "tshirt_table_cdk",
                partition_key=dynamodb_cdk.Attribute(name="tshirt_id", 
                type=dynamodb_cdk.AttributeType.STRING))
        
        # Create Lambda function
        # TODO: Move lambda to consolidated implementation across stack samples
        tshirt_lambda_cdk = lambda_cdk.Function(self, "tshirt_lambda_cdk",
                code=lambda_cdk.Code.from_asset("./tshirt_lambda/"),
                handler="tshirt_lambda.lambda_handler",
                runtime=lambda_cdk.Runtime.PYTHON_3_9)
        tshirt_lambda_cdk.add_environment("TABLE_NAME", tshirt_table_cdk.table_name)


        # Grant Lambda permission to access DynamoDB
        tshirt_table_cdk.grant_write_data(tshirt_lambda_cdk)

        # Create ApiGateway, point to above Lambda
        tshirt_api_cdk = apigateway_cdk.LambdaRestApi(self, "tshirt_api_cdk",
                        handler = tshirt_lambda_cdk,
                        proxy = False)
        customer_api_resource = tshirt_api_cdk.root.add_resource('add_tshirt')
        customer_api_resource.add_method("POST")
